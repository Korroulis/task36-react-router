import React from 'react';
import RegisterForm from '../forms/RegisterForm';
import {Link, useHistory} from 'react-router-dom';

const Register = () => {
    
    const history = useHistory();

    const handleRegisterClicked = (result) => {
        console.log('Triggered from the form',result);
        if (result) {
            history.replace("/dashboard");
        }
    };


    return (
        <div>
            <h1>Register to survey puppy</h1>
            
            <RegisterForm complete={ handleRegisterClicked}/>
            {/* <a href={LoginForm}>Already registered???</a> */}
            
            <Link to="/login" >
                Already registered? Log in
            </Link>
        </div>
)
}

export default Register;