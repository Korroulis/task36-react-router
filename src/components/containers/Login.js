import React from 'react';
import LoginForm from '../forms/LoginForm';
import {Link} from 'react-router-dom';

const Login = () => {
    return (
        <div>
            <h1>Please login to the survey puppy</h1>

            <LoginForm />
            <Link to="/register">
                Don't have an account? Register here
            </Link>
        </div>
    )
    
};

export default Login;