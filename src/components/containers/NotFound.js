import React from 'react';
import {Link} from 'react-router-dom';

const NotFound = () => {
    return (
        <div className="NotFound">
            <h1>Page not found</h1>
            <img src="http://nerdreactor.com/wp-content/uploads/2012/12/Link.jpg" alt="Not found" width="300"/>
            <br/>
            <br/>
            <Link to="/login">
                Go to Login
            </Link>
        </div>
    )
};

export default NotFound;