import React, { useState } from 'react';
import {registerUser} from '../../api/user.api';

const RegisterForm = props => {

    const [username,setUsername]=useState('');
    const [password,setPassword]=useState('');
    const [isRegistering,setIsRegistering] = useState(false);
    const [registerError,setRegisterError] = useState('');
   const [Registered,setRegistered] = useState(false);

    const onRegisterClicked = async ev => {

        console.log(ev);
        setIsRegistering(true);
        console.log(username,password); 
        let result;

        try {
            const {status} = await registerUser(username,password);
            result = status === 201;
        } catch (e) {
            setRegisterError(e.message || e);
           /*  if (e) {
                setRegistered(false);
            }
            else if (!e) {
                setRegistered(true);
            } */
        } finally {
            setIsRegistering(false);
            setRegistered(true);
            props.complete(result);
            
        }

    }

    const onUsernameChanged = ev => setUsername(ev.target.value.trim());
    const onPasswordChanged = ev => setPassword(ev.target.value.trim());

    return (
        <form style={{marginTop: 3 + 'em' }}>
            <div className="form-group">
                <label className="col-l-2 col-form-label">Email</label>
                <div className="col-sm-10">
                <input type="email" className="form-control" onChange={onUsernameChanged}/>
                </div>
            </div>
            <div className="form-group">
                <label className="col-l-2 col-form-label">Password</label>
                <div className="col-sm-10">
                <input type="password" className="form-control" onChange={onPasswordChanged}/>
                </div>
            </div>
            <div className="form-group">
                <label className="col-l-2 col-form-label">Confirm Password</label>
                <div className="col-sm-10">
                <input type="password" className="form-control" />
                </div>
            </div>
            <div style={{marginTop: 4 + 'em', marginBottom: 2 + 'em'  }}>
                <button type="button" className="btn btn-primary" onClick={onRegisterClicked}>Register</button>
            </div>

            {isRegistering && <div>Registering User...</div>}

            {registerError && <div>{registerError}</div> }

            {!registerError && Registered && <div>User registered successfully</div>}

        </form>
    );
    
}

export default RegisterForm;