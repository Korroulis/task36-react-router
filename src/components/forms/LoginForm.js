import React from 'react';

const LoginForm = () => {
    return (
        <form style={{marginTop: 3 + 'em' }}>
            <div className="form-group">
                <label className="col-l-2 col-form-label">Email</label>
                <div className="col-sm-10">
                <input type="email" className="form-control" id="Email"/>
                </div>
            </div>
            <div className="form-group">
                <label className="col-l-2 col-form-label">Password</label>
                <div className="col-sm-10">
                <input type="password" className="form-control" id="Password"/>
                </div>
            </div>

            <div style={{marginTop: 4 + 'em', marginBottom: 2 + 'em' }}>
                <button type="submit" className="btn btn-success">Log in</button>
            </div>
        </form>
    )
}

export default LoginForm;