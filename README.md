# Task 36: React Router

Take the current project and implement routing

* Install the React Router Dom.
* Define be 3 main routes: Login, Register and Dashboard.
* The first screen that must be shown is the Login. 
* The user must be able to navigate from the Login to the Register page.
* On successful Registration, the user must be navigated to the Dashboard programatically.
* Create a 404 Not Found page that is displayed when the user types an unknown path in the address bar of the browser.
* The 404 can simply display any image. Checkout Freepik.com for some fun images.